import React, { Component } from 'react';
import Header from './components/Header'
import SubmitForm from './components/SubmitForm'
import TodosList from './components/TodosList'
import { API, graphqlOperation } from 'aws-amplify'
import { createTodo, updateTodo, deleteTodo } from './graphql/mutations'
import { listTodos } from './graphql/queries'


class App extends Component {
  state = {
    tasks: [],
    todosCompleted: 0,
    todosLeft: 0,
    inputValue: '',
    count: 0,
  }

  componentDidMount() {
    this.fetchTodos()
    // console.log('todosLeft in componentDidMount:', todosLeft)
  }

  fetchTodos = async () => {
    try {
      const todoData = await API.graphql(graphqlOperation(listTodos))
      const todos = todoData.data.listTodos.items
      // console.log('todos in fetchTodos', todos)

      var todosCompleted = todos.filter(ele => ele.completed).length
      var todosLeft = todos.length - todosCompleted

      var compare = (a, b) => (a.index > b.index) ? 1 : ((b.index > a.index) ? -1 : 0)
      todos.sort(compare);
      var finalTodo = todos[todos.length - 1].index
      console.log('fetch finalTodo:', todos[todos.length - 1])
      console.log('fetch finalTodo index:', finalTodo)

      this.setState({ tasks: todos, todosCompleted: todosCompleted, todosLeft: todosLeft, count: finalTodo + 1 }) //? set the count in state to last todo's index (should be the highest number) so that count is dynamically continuous forever
    }
    catch (err) {
      console.log(`error fetching todos. there probably just aren't any in the database yet`, err)
    }
    console.log('this.state END FETCH', this.state)
  }


  addTodo = async (e) => {
    e.preventDefault();
    try {
      var newTodo = this.state.inputValue
      if (newTodo === '') return;
      var count = this.state.count


      const response = await API.graphql(graphqlOperation(createTodo, { input: { name: newTodo, index: count, completed: false } }))
      var resTodo = response.data.createTodo
   
      this.setState({ tasks: [...this.state.tasks, { id: resTodo.id, name: resTodo.name, index: resTodo.index, completed: resTodo.completed }], todosLeft: this.state.todosLeft + 1 })

      this.setState({ inputValue: '', count: count + 1 });
      document.getElementById("submitInput").value = "";
    }
    catch (err) {
      console.log('Error creating todo:', err)
    }
  }


  //? HANDLERS FOR ADD/COMPLETE/DELETE TASKS
  handleChange = e => {
    var todo = e.target.value
    this.setState({ inputValue: todo })
  }


  completeTask = async (e) => {
    // var index = e.target.dataset.index
    var id = e.target.id

    const tempArr = [...this.state.tasks];
    var foundIndex = tempArr.findIndex(todo => todo.id === id)
    tempArr[foundIndex].completed = !tempArr[foundIndex].completed
    var completeInDB = tempArr[foundIndex].completed

    var todosCompleted = tempArr.filter(ele => ele.completed).length
    var todosLeft = tempArr.length - todosCompleted

    // console.log('In completeTask Total/Completed/Left:', this.state.tasks.length, todosCompleted, todosLeft)


    this.setState({ tasks: tempArr, todosCompleted: todosCompleted, todosLeft: todosLeft });
    await API.graphql(graphqlOperation(updateTodo, { input: { id: id, completed: completeInDB } }))
  }


  handleDelete = async (props) => {
    // debugger
    console.log('props in delete', props)
    var id = props.id

    const tempArr = [...this.state.tasks];
    var tempCompleted = this.state.todosCompleted
    var tempLeft = this.state.todosLeft
    var foundIndex = tempArr.findIndex(todo => todo.id === props.id)

    //* Ensure number displayed and number of completed/leftToComplete in render/state are matching
    var checkCompleted = tempArr[foundIndex].completed === true ? (tempCompleted - 1) : tempCompleted
    var checkLeft = tempArr[foundIndex].completed === true ? (tempLeft) : (tempLeft - 1)

    // tempArr[foundIndex].completed = !tempArr[foundIndex].completed
    tempArr.splice(foundIndex, 1);

    this.setState({ tasks: tempArr, todosCompleted: checkCompleted, todosLeft: checkLeft });
    await API.graphql(graphqlOperation(deleteTodo, { input: { id: id } }))

    console.log('DELETE Total/Completed/Left:', this.state.tasks.length, this.state.todosCompleted, this.state.todosLeft)
  }



  render() {
    return (
      <div className='wrapper'>
        <div className='card frame'>
          <Header todosCompleted={this.state.todosCompleted} todosLeft={this.state.todosLeft} numTasks={this.state.tasks.length} />
          <TodosList tasks={this.state.tasks} onDelete={this.handleDelete} completeTask={this.completeTask} />
          <SubmitForm addTodo={this.addTodo} handleChange={this.handleChange} />
        </div>
      </div>
    );
  }
}

export default App;