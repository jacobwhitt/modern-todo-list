import React from 'react'
import Todo from './Todo'

const TodosList = (props) => {
    console.log('props in TodosList:', props)

    //? props.tasks is from this.state.tasks
    const todos = props.tasks.map((todo, index) => {
        return <Todo content={todo} key={todo.id} onDelete={props.onDelete} completeTask={props.completeTask} />
    })
    return (
        <div className={props.tasks.length === 0 ? 'list-hidden' : 'list-wrapper'}>
            {todos}
        </div>
    );
}

export default TodosList;