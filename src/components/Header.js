import React from 'react';

const Header = (props) => {
    // console.log('props in header', props)
    return (
        <div className='card-header'>


            {props.numTasks === props.todosCompleted && props.numTasks !== 0
                ? <>
                    <h1 className='card-header-title header'>You completed all your tasks!</h1>
                    <h3>Hey, nice job!</h3>
                </>
                : <>
                    <h1 className='card-header-title header'>
                        {/* //* CHECK NUMBER OF TODOS, MAKE NOUN PLURAL OR NOT  */}
                        You have {props.todosLeft ?
                            props.todosLeft + ` task${props.todosLeft > 1 ? 's to complete' : ' to complete'}`
                            : props.todosLeft === 0 ? 'nothing to do?' : ''}
                    </h1>

                    <h3 className='card-header-title header'>
                        {props.numTasks ? null : 'Make a list and get to work!'}
                        {props.numTasks >= 5 && props.numTasks < 10 ? 'Someone\'s busy...' : props.numTasks >= 10 ? 'Time to hire a PA, dude' : null}
                    </h3>
                </>
            }

        </div>
    )
}

export default Header;