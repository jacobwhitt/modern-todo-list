import React from 'react'

const Todo = (props) => {
    console.log('props in Todo container', props.content)

    return (
        <div className='list-item'>
            <div className={props.content.completed ? 'list-item-completed' : 'list-item-uncompleted'}>
                <h3>{props.content.name.charAt(0).toUpperCase() + props.content.name.slice(1)
                }</h3>
            </div>


            <div className='list-item-delete'>
                <input type="checkbox" id={props.content.id} defaultChecked={props.content.completed}
                    onChange={props.completeTask} />
                
                <button className="delete is-large" onClick={() => { return props.onDelete(props.content) }}>delete</button>
            </div>

        </div>
    );
}

export default Todo;