import React from 'react';

const SubmitForm = (props) => {
// console.log('submit form props', props)
    
    return (
        <form onSubmit={props.addTodo}>
            <input
                id='submitInput'
                type='text'
                className='input'
                placeholder='Enter a task'
                value={props.todo}
                onChange={props.handleChange}
            />
            <button className='button'>Submit</button>
        </form>
    );
}


export default SubmitForm;