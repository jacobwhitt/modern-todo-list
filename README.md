Welcome to my Modern Todo List!

You may begin by adding tasks in the input and submitting them with the button provided.

The messages above the Todo List container will dynamically update based on the list's content.

Tasks can be completed with the check mark or deleted with the delete button.

Completing all tasks displays a congratulatory message and a 'clear all' button.

Send me a message here on gitlab or at jt@berg-whitt.com for any issues that arise.